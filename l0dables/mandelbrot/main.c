#include "epicardium.h"

#include <math.h>
#include <stdio.h>

#define LIMIT(x, min, max) (((x) < (min)) ? (min) : (((x) > (max)) ? (max) : (x)))
#define MAX(x, max) ((x) > (max) ? (max) : (x))

#define W 160
#define H 80

typedef struct
{
   float r;
   float i;
} complex;

typedef struct
{
   complex c;      // center
   float s;        // scale
   uint16_t mi;    // max iterations
   uint16_t ipf;   // iterations per frame
   uint16_t si;    // start iteration for color scheme
   uint16_t ei;    // end iteration for color scheme
} point;

static const point points[] = {
   {{ -1.76f,  0.0f}, 0.06f,  500, 1,  1, 250},
   {{ 0.351423f,  0.060397f}, 0.00086f,  500, 1,  10, 125},
   {{ -1.0740916f,  -0.24611819f}, 0.0005586f,  500, 1,  20, 425},
/*
   {{ 0.378765046933103,  0.099367336526827}, 0.000000016660047,  3000, 10,  800, 2000},
   {{ 0.270198814200415,  0.004681968467986}, 0.000000113151886, 20000, 80,  300, 5000},
   {{-0.746074740341831, -0.075444427906717}, 0.000012327288166, 20000, 80, -600, 5000},
   {{-0.198253016884858, -1.100946583302435}, 0.000000000178841,  8000, 25, 1000, 3500},
*/
};

static complex cstore[W * H];
static uint16_t iter;
static uint8_t pi = 0;

union disp_framebuffer fb;

static complex f(complex x, complex c, float *sqr)
{
   // complex numbers: x * x + c;
   complex res;
   float r2 = x.r * x.r;
   float i2 = x.i * x.i;
   res.r = r2 - i2 + c.r;
   res.i = 2 * x.r * x.i + c.i;
   *sqr = r2 + i2;
   return res;
}

static uint16_t color(uint16_t iter)
{
   float r, g, b, c;
   uint8_t red, green, blue;
   uint16_t res;

   c = ((float)iter - points[pi].si) / (points[pi].ei - points[pi].si);
   r = 1.0f * c;
   g = 2.5f * c;
   b = 8.0f * c;

   red   = LIMIT(r, 0.0f, 1.0f) * ((1 << 5) - 1);
   green = LIMIT(g, 0.0f, 1.0f) * ((1 << 6) - 1);
   blue  = LIMIT(b, 0.0f, 1.0f) * ((1 << 5) - 1);
   
   res = (red << 3) | ((green & 0x38) >> 3) | ((green & 0x7) << 13) | (blue << 8);

   return res;
}

void reset()
{
   for (uint8_t y = 0; y < H; ++y)
   {
      uint16_t index = y * W;
      for (uint8_t x = 0; x < W; ++x)
      {
         cstore[index].r = 0.0f;
         cstore[index++].i = 0.0f;
         fb.fb16[y][x] = 0;
      }
   }
   iter = 0;
   pi = (pi + 1) % (sizeof(points) / sizeof(point));
}

void init()
{
   epic_disp_open();
   reset();
}

void fps()
{
  static uint8_t frames = 0;
  static uint64_t last = 0;
  if(frames++ == 100)
  {
    uint64_t now = epic_rtc_get_milliseconds();
    printf("%ld fps\n", 100000 / (uint32_t)(now - last));
    last = now;
    frames = 0;
  }
}

bool draw()
{
   for (uint8_t y = 0; y < H; ++y)
   {
      uint16_t index = y * W;
      for (uint8_t x = 0; x < W; ++x)
      {
         if (fb.fb16[H-y][W-x] == 0)
         {
            uint16_t localiter = 0;
            complex c = points[pi].c;
            c.r += (x - W / 2) * points[pi].s / H;
            c.i += (y - H / 2) * points[pi].s / H;
            while (fb.fb16[H-y][W-x] == 0 && localiter < points[pi].ipf)
            {
               float sqr;
               cstore[index] = f(cstore[index], c, &sqr);
               localiter++;
               if (sqr >= 4.0f)
               {
                  fb.fb16[H-y][W-x] = color(iter + localiter);
               }
            }
         }
         index++;
      }
   }
   epic_disp_framebuffer(&fb);

   iter += points[pi].ipf;
   if (iter > points[pi].mi)
   {
      return false;
   }
   return true;
}

int main(void)
{
  init();
  for(;;)
  {
    if(!draw())
    {
      reset();
    }
    fps();
  }
}

